# Adapt translation extract

this repo aims to ease translation of Adapt' s modules.
It was developed as a quick and dirty extractor

currently these are notes for the development of this module

# usage
for the moment this is a 2 step process

## extraction 

- ```cd``` to the script directory
- copy the content of the ```course``` folder of the ADAPT course to a folder named ```src``` (create it in the root dir of this script
- in the root of the script dir run ```npm run extract```

this will treats :

- article
- config.json
- course.json
- contentObjects.json
- components.json

and create a ```extracted``` folder at the root of the script's directory 
containing MODIFIED version of the json files with reference to the translatable fields
and a ```trad.json``` file.

KEEP THESE FILES TOGHTHER !!! order of the translated fields IS important so a trad.json is linked to the ADAPT's json files, and modification to the main files may end up genrating different ids

## write trads

```node wrTrad```

will take the content of ```extracted``` and generate an output in the ```dest``` directory

modification of the trad.json file is left to the script user since the translation workflow is specific to each projects

# goals/task  
this tool should:

- [x]read the needed source files
- [x]extract needed fields
  - [x]store the field
  - [x]create field object containing
    - [x]a uId
    - [x]the original field key
    - [x]the original field content
- [x] replace extracted fields content with a template sting containing the field uId
- [x] write the extracted fields as a json object
- [x] read / write csv <=> json for the translators
- [x] write the source file with modified fields
- write a full model for the input, output and translations, for the next step (at work in the objectify branch)
- write objects to a database (level db)
- translation via object rather than lodash templates ?
- more exports formats (at work, XLS support)
- build an ui for translation input
- make config more clear and centralised
- write clean documentation
- harmonize trad object outputs
- automate process
- [x] have some tests 

# SHORT TERMS TODO
- finishing writing tests
- [x] treat special case of the hotgraphic and textinput (see git repo examples)
- finish all types of components
- [x]add specific cases for files other than component (currently treated by generic case)
- switch to streams
- write schema with all translated properties specified
- switch all debugging to use debug module
- create a better naming scheme for the translation keys
- switch translation writting from file based to object based
- write a script to reorder components
- make a better build process
  - use excel file instead of csv
  - steamline axa's csv production
  - integrate web vtt production to the build (and output files in the dest files as well)

# notes

this system is useless now because of adapt system, but need improvement for legacy project, no need to go to far tho
data extractions functions are in the ```lib/transformers.js``` folder  
cases for each type of objects are located in the ```lib/cases``` folder

this is an internal tool, that is not yet in a polished state, use at your own risks!!!  
A id based key is at work but for now this require stable json files since adding item may offset extracted translations keys.
