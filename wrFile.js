var fs = require('fs-extra')
var _ = require('lodash')
var path = require('path')
var nb = require('node-beautify')
var fileP = process.argv[2] || './extracted/trad/trad.json'
console.log(fileP)
var trad = fs.readJSONSync(fileP, 'utf8')

var tradObj = {}

function makeTrO (arr, obj) {
  _.forEach(arr, function (o, k) {
    _.forEach(o, function (v, k) {
      console.log('v', v, 'k', k)
      obj[k] = v
    })
  })
  // console.log(obj)
  replaceObj('config.json', obj)
  replaceObj('course.json', obj)
  replaceObj('components.json', obj)
  replaceObj('contentObjects.json', obj)
  return obj
}

makeTrO(trad, tradObj)
// apply lodash template to get tranlted json files
// function replaceEntry (file, data) {
//   var tempPath = path.join(__dirname + '/dist/' + file)
//   var destPath = path.join(__dirname + '/dest/' + file)
//   var template = _.template(fs.readFileSync(tempPath, 'UTF8'))
//   var rendered = template(data)
//   fs.writeFile(destPath, nb.beautifyJs(rendered), function (err, data) {
//     if (err) {console.log(err)} else {
//       console.log('written dest file', file)
//     }
//   })

//   return template(data)
// }
function gfObj (file) {
  return fs.readFileSync(path.join(__dirname, '/extracted/', file), 'UTF8')
}

function replaceObj (file, data) {
  var destPath = path.join(__dirname, '/dest/', file)
  var template = _.template(gfObj(file))
  var rendered = template(data)
  fs.writeFile(destPath, nb.beautifyJs(rendered), function (err, data) {
    if (err) { console.log(err) } else {
      console.log('written dest file', file)
    }
  })

  return template(data)
}
// function makeDataObj (dataArr) {
//   var cArr = JSON.parse(dataArr)
//   var dObj = {}
//   cArr.forEach(function (element, index) {
//     dObj[element.key] = element.data
//   })
//   return dObj
// }

// // test replace entry
// var testu =  makeDataObj(fs.readFileSync('./dist/trad.txt','utf8'))
// var testu =  makeDataObj(fs.readFileSync('./dist/trad.txt','utf8'))
// //
// console.log(testu)

// console.log(replaceObj('config.json',trad))
