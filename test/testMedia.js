var funcName = 'media'
var fs = require('fs-extra')
var path = require('path')
var test = require('tape')

var filePath = './testFiles/samples/'
var testData = fs.readJsonSync(
  path.join(__dirname, filePath, funcName + '.json'), 'utf8')
var conf = {
  o: testData,
  t: []
}

var transform = require('../lib/cases/' + funcName)
var testObj = transform(conf)

var Validator = require('jsonschema').Validator
var valFunc = new Validator()
var schema = fs.readFileSync(path.join(__dirname, '../schema/', funcName + '.schema'))
var schResults = valFunc.validate(testObj.o, schema)

var commonTests = require('./commonTests')
var isValidTemplate = commonTests.isValidTemplate

function runTests () {
  test('checking component type ' + funcName, function (t) {
    t.plan(2)
    t.equal(testObj.o._type, 'component')
    t.equal(testObj.o._component, funcName)
  })

  test('schema output test', function (t) {
    t.plan(2)
    t.equal(Array.isArray(schResults.errors), true)
    t.equal(schResults.errors.length, 0)
  })
  test('translated fields are valid lodash template test', function (t) {
    t.plan(3)
    isValidTemplate(testObj.o.title, t)
    isValidTemplate(testObj.o.displayTitle, t)
    isValidTemplate(testObj.o.body, t)
  })
// test.onFinish(finished)
}
runTests()

var config = require('../config')
var tPath = config.paths.testP

fs.ensureDir(__dirname + tPath, function (err) {
  if (err) console.log(err)
  fs.outputJsonSync(path.join(__dirname, tPath, funcName + '.json'), testObj)
})

module.exports = runTests
