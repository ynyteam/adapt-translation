var funcName = 'makeUId'
var test = require('tape')
var randomstring = require('randomstring')

var helpers = require('../lib/helpers')
var makeUId = helpers.makeUId

function makeRString () {
  return randomstring.generate({
    length: 12,
    charset: 'alphabetic'
  })
}

function testUId (t) {
  var testObj = {}
  var testString = makeRString()
  var uID = makeUId(testString)
  // test if the sting passed is unchanged
  var str = uID.substring(0, testString.length)
  t.equal(testString, str, 'str matching' + str)

  testObj.baseString = testString
  testObj.uId = uID
  // testObj.strNum = strNum
  // console.dir(testObj)
}

function runTests () {
  test('batch check ' + funcName, function (t) {
    t.plan(1)
    testUId(t)
  })
}

runTests()
