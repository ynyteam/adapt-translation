var debug = require('debug')('extract')
// var name = 'extract JS'

var fs = require('fs-extra')
var path = require('path')
var _ = require('lodash')
var toCsv = require('./lib/toCsv')

var cs = require('./lib/loadCases')

var i = require('./lib/majordome')
var green = i.green
var red = i.red
var magenta = i.magenta
var buildColor = i.buildColor
var checkDirs = i.cd

fs.ensureDirSync('./extracted')

const extractMod = require('./extractMod')

const transPage = extractMod.transPage
const transArt = extractMod.transArt
const transConf = extractMod.transConf
const transCourse = extractMod.transCourse
const transComps = extractMod.transComps

const tArr = []
// TODO get rid of this helper func everywhere
// var wrT = h.wrT

var curCourse = process.argv[2] || 'course' // dirname for the source, located in /src providing default or using the first provided arg
var dest = process.argv[3] || './extracted/' // same as abaos for output dir and 3rd param

// console.log(magenta('booting', name))
// ensure needed dirs are presents, create them if needed
checkDirs()

// loading the required jsons to translate
var artObject = fs.readJsonSync(path.join(__dirname, 'src/' + curCourse + '/en/articles.json'))
var courseObject = fs.readJsonSync(path.join(__dirname, 'src/' + curCourse + '/en/course.json'))
var componentObject = fs.readJsonSync(path.join(__dirname, 'src/' + curCourse + '/en/components.json'))
var contentObject = fs.readJsonSync(path.join(__dirname, 'src/' + curCourse + '/en/contentObjects.json'))
var confObject = fs.readJsonSync(path.join(__dirname, 'src/' + curCourse + '/config.json'))

debug(magenta(('Starting directory: ' + process.cwd())))

function buildAll (csv) {
  debug(magenta(('Cleaning previous build ')))
  i.bal(['extracted'])

  console.log(buildColor('building config.json'))
  transConf(confObject, tArr)
  console.log(buildColor('building course.json'))
  transCourse(courseObject, tArr)
  console.log(buildColor('building articles.json'))
  transArt(artObject, tArr)
  console.log(buildColor('building contentObjects.json'))
  transPage(contentObject, tArr)
  console.log(buildColor('building components.json'))
  transComps(componentObject, tArr)

  var outPath = dest + curCourse
  fs.ensureDirSync(path.join(dest + '/trad/'))
  fs.ensureDirSync(path.join(outPath))
  fs.ensureDirSync(path.join(outPath + '/en/'))
  fs.writeJsonSync(path.join(outPath + '/config.json'), confObject)
  fs.writeJsonSync(path.join(outPath + '/en/articles.json'), artObject)
  fs.writeJsonSync(path.join(outPath + '/en/course.json'), courseObject)
  fs.writeJsonSync(path.join(outPath + '/en/contentObjects.json'), contentObject)
  fs.writeJsonSync(path.join(outPath + '/en/components.json'), componentObject)
  fs.writeJsonSync(path.join(dest + '/trad/' + '/trad.json'), tArr)

  if (csv) {
    toCsv(tArr, curCourse)
  }
}
buildAll(true)

module.exports = {
  buildAll: buildAll
}
