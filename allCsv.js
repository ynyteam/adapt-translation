const path = require('path')
const stringify = require('csv-stringify')
const fs = require('fs-extra')
const _ = require('lodash')

var filesArr = [
  {name: 'article', path: '/out/article/allTradLinesarticle.json'},
  {name: 'blocks', path: '/out/blocks/allTradLinesblocks.json'},
  {name: 'components', path: '/out/components/allTradLinescomponents.json'},
  {name: 'config', path: '/out/config/allTradLinesconfig.json'},
  {name: 'contentObjects', path: '/out/contentObjects/allTradLinescontentObjects.json'},
  {name: 'course', path: '/out/course/allTradLinescourse.json'}
]

fs.ensureDirSync('./out/csv')

function extractdata () {
  _.each(filesArr, function (file) {
    var entriesArr = []
    var parsedObj = fs.readJsonSync(path.join(__dirname, file.path))
    if (parsedObj.length === 0) { return console.log('empty file trad', file.name) }
    console.log('file', file.name, 'has', parsedObj.length, 'entries')
    _.each(parsedObj, function (entry) {
      var tempsObjArr = []
      tempsObjArr.push(Object.keys(entry)[0])
      tempsObjArr.push(entry[Object.keys(entry)])
      entriesArr.push(tempsObjArr)
    })
    stringify(entriesArr, function (err, output) {
      if (err) throw err
      fs.writeFileSync(path.join(__dirname, '/out/csv/', file.name + '.csv'), output, 'utf-8')
    })
  })
}

extractdata()
