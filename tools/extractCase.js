var fs = require('fs-extra')
var _ = require('lodash')
var path = require('path')
var objectPath = require('object-path')

var i = require('../lib/majordome')
var green = i.green
var red = i.red
// var magenta = i.magenta
var blue = i.buildColor
var target = 'matching'
// var checkDirs = i.cd

var compTypeArray = [
  'accordion',
  'graphic',
  'hotgraphic',
  'matching',
  'mcq',
  'media',
  'narrative',
  'text',
  'textinput'
]

var componentList = [
  '../src/course-00/en/components.json',
  '../src/course-01/en/components.json',
  '../src/course-02/en/components.json',
  '../src/course-03/en/components.json',
  '../src/course-04/en/components.json'
]

function loadFile (file) {
  return fs.readJsonSync(path.join(__dirname, file), 'utf8')
}

/**
 * test if a provided sting match a type of component
 * @param  {str} itemType       type to compare to the list
 * @param  {arr} compList       list of type
 * @return {sting/bool}         the type or false
 */
function matchComp (itemType, compList) {
  var testMatch = compList.filter(function (v, k) {
    if (v === itemType) return true
  })

  if (testMatch.length) {
    return testMatch[0]
  } else {
    return false
  }
}

/**
 * counts the types in the items of a collection
 * @param  {obj/arr} col     collection to iterate
 * @param  {array}   typesArr array containing strings with all known types
 * @param  {array}   result array containing strings with all known types
 * @return {[type]}          [description]
 *
 */
function countTypes (col, typesArr, results, WRT) {
  // populate result obj with types if new
  if (!results.media) {
    console.log(blue('init result object'))
    _.forEach(typesArr, function (iType) {
      results[iType] = 0
    })
    results.saved = {}
  }

  var hasNewTypes = false
  var newTypes = []
  var saved = []

  _.forEach(col, function (item, key) {
    var itemType = objectPath.get(item, '_component')

    if (matchComp(itemType, typesArr) === false) {
      hasNewTypes = true
      console.log(red('unkown'), itemType)
      results[itemType] = 1
      newTypes.push(itemType)
    }

    if (itemType === target) {
      saved.push(item)
    }

    _.forEach(typesArr, function (v, k) {
      if (v === itemType) {
        // if it is the first of the kind
        if (results[v] === 0) {
          console.log(red('first', v))
          var fPath = './testFiles/samplesArr/' + v + '.json'
          var arr = []
          arr.push(item)
          if (WRT) fs.outputJsonSync(fPath, arr)
        }
        ++results[v]
        return false
      }
    })
  })
  // objectPath.set('results.saved', target, saved)
  // results.saved[target] = saved
  // fs.appendFileSync('matching.json', JSON.stringify(saved))
  if (hasNewTypes) {
    results.newTypes = newTypes
    fs.appendFileSync('newTypes.json', newTypes.toString() + ', ')
  }
  if (WRT) {
    fs.outputJsonSync('countResult.json', results)
    console.log(green('results'), results)
  }
  console.log(results)
  return results
}

/**
* iterate a collection and exports the items matching the
* components types _component key
* @param  {obj/arr}      col    collection to iterate
* @param  {string,col}   type   type or array of types to match
* @param  {array}        outArr array to push matches to
* @return {[type]}       outArr
*
* extractObj(extArray, 'text', typedColArr)
*
*/
function countAll (col, WRT) {
  var results = {}
  _.forEach(col, function (file) {
    console.log(blue('scanning', file))
    countTypes(loadFile(file), compTypeArray, results, WRT)
  })
  console.log(green('Final results'), results)
  fs.outputJsonSync('countALLResult.json', results)
}

function countFile (file, WRT) {
  var results = {}
  countTypes(file, compTypeArray, results, WRT)
}

// countAll(componentList)
// function extractObj (col, type, outArr) {
//   outArr = typeof outArr !== 'undefined' ? outArr : []
//   _.forEach(col, function (item) {
//     var itemType = objectPath.get(item, '_component')
//     if (itemType === type) {
//       console.log("it' a match", itemType)
//       outArr.push(item)
//       return false
//     }
//   })
//   return outArr
// }

// var typedColArr = []
// extract from multiple files
// function extractALL (col, type, outArr) {
//   _.forEach(col, function (file, key) {
//     console.log('openening', green(file))
//     var data = loadFile(componentList[key])
//     _.forEach(data, function (obj) {
//       console.log(obj)
//       extractObj(obj, type, typedColArr)
//     })
//     countTypes(file, compTypeArray)
//   })
// }
//
// function writeExtracted (data, filename) {
//   if (EXTRACT && typedColArr.lenght > 0) {
//     console.log('extracted vals', typedColArr)
//     fs.outputJsonSync('extracted.json', typedColArr)
//   }
// }

function countData (file, WRT) {
  var results = {}
  // console.log(compTypeArray, results)
  countTypes(file, compTypeArray, results, WRT)
}

countAll(componentList, true)

module.exports = {
  matchComp: matchComp,
  countTypes: countTypes,
  countAll: countAll,
  countData: countData,
  countFile: countFile

}
