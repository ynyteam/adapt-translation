'use strict'
/* make AXA XLS
this is a rudimentary tool to make Ofelia Repes happy
it take an XLS file provided as the Variable 'workbook' and out put a 'out.xlsx' file,
stripped of the HTML as some rendered formatted plain text with a fixed width of
*/

const debug = require('debug')('cleanAxa')
const XLSX = require('xlsx')
const fileName = process.argv[2] || 'mod9es.xlsx'
const outName = process.argv[3] || 'out.xlsx'
const workbook = XLSX.readFile(fileName)

const htmlToText = require('html-to-text')

const sheetNameList = workbook.SheetNames
// sheetNameList.forEach(function (y, i) {
//   console.log(y, i)
// })

// console.log(sheetNameList)
// var allData = []
/* function to convert all string of non empty cell from html => plain text */
function toText (sheet) {
  for (var z in sheet) {
    /* all keys that do not begin with "!" correspond to cell addresses */
    const numberPattern = /\d+/g
    var toCellNum = 'D' + z.match(numberPattern)
    debug('toCellNum:', toCellNum)

    if (z[0] === '!') continue
    if (z[0] === 'C') {
      let cVal = sheet[z].v
      // console.log(sheet[z].v)
      // console.log(cVal)
      // const re = new RegExp('&nbsp;', 'g')
      // const re = new RegExp(String.fromCharCode(160), 'g')
      cVal = htmlToText.fromString(cVal)
      cVal = cVal.replace(/\\n\\n/g, ' ').replace(/\\n/g, ' ').replace(/\r\n\r\n/g, ' ').replace(/\r\n/g, ' ')
      debug('mod val', cVal)
      sheet[z].v = htmlToText.fromString(sheet[z].v) // convert Html to
    }
  //	.replace(/\[([^]]+)\]/g,'') // stip image tag from the conversion
  }
// console.log(allData)
// return allData
}

/* iterate through sheets */
sheetNameList.forEach(function (y, i) {
  var worksheet = workbook.Sheets[y]
  console.log(y)
  switch (y) {
    case 'AdaptText':
      toText(worksheet)
      console.log('components spotted')
      break
    default:
      console.log('deleted', y)
      delete workbook.Sheets[y]
      sheetNameList.splice(i, 1)
      return null
  }
})
/* write down the sheet */

function writeThingy (workbook, outName) {
  debug('write thingy')
  XLSX.writeFile(workbook, outName)
}
writeThingy(workbook, outName)
