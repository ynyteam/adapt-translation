/* make AXA XLS
this is a rudimentary tool to make Ofelia Repes happy
it take an XLS file provided as the Variable 'workbook' and out put a 'out.xlsx' file,
stripped of the HTML as some rendered formatted plain text with a fixed width of
*/

var XLSX = require('xlsx')
var fileName = process.argv[2] || 'mod5.xlsx'
var outName = process.argv[3] || 'out.xlsx'
var workbook = XLSX.readFile(fileName)

var sheetNameList = workbook.SheetNames

console.log(sheetNameList)
// var allData = []
/* function to convert all string of non empty cell from html => plain text */
function toText (sheet) {
  for (var z in sheet) {
    /* all keys that do not begin with "!" correspond to cell addresses */
    if (z[0] === '!') continue
    if (z[0] === 'A' || z[0] === 'B' || z[0] === 'C') {
      sheet[z].h = undefined
      var str = sheet[z].v
      // sheet[z].v = sheet[z].v + '\r\nyo yo'
      // console.log(typeof(sheet[z].v))
      if (typeof sheet[z].v === 'string') {
        sheet[z].v = str.replace(/\r\n\r\n/g, ' ').replace(/\r\n/g, ' ').replace(/\\n\\n/g, ' ').replace(/\\n/g, ' ').replace(/\[([^]]+)\]/g, '')
      } else { console.log(typeof sheet[z].v, sheet[z].v) }
    }
    // console.log(sheet[z])
    // if (z[0] === 'F' || z[0] === 'E') {
    //   // console.log(sheet[z].v)
    // }
  // sheet[z].v = htmlToText.fromString(sheet[z].v) // convert Html to
  //	.replace(/\[([^]]+)\]/g,'') // stip image tag from the conversion
  }
// console.log(allData)
// return allData
}

/* iterate through sheets */
sheetNameList.forEach(function (y, i) {
  var worksheet = workbook.Sheets[y]
  if (y === 'components') {
    toText(worksheet)
  }
})
/* write down the sheet */
XLSX.writeFile(workbook, outName)
