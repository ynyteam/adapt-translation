const XLSX = require('xlsx')
const path = require('path')
const _ = require('lodash')
const fs = require('fs-extra')

const fileName = process.argv[2] || path.join(__dirname, '../extracted/mod9EsAdapt.xlsx')
const outName = process.argv[3] || 'out.xlsx'
const workbook = XLSX.readFile(fileName)

const extractedObj = []

const sheetNameList = workbook.SheetNames
console.log(sheetNameList[0])
const jsonData = XLSX.utils.sheet_to_json(workbook.Sheets[sheetNameList[0]])

// console.log(jsonData)

_.each(jsonData, (d, k, i) => {
  var entry = {}
  entry[d['référence']] = d['texte traduit']
  extractedObj.push(entry)
  console.log(entry)
})

fs.writeJsonSync(path.join(__dirname, '../extracted/trad.json'), extractedObj)
