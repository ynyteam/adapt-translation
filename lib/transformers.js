var _ = require('lodash')
var h = require('./helpers')

var getKey = h.getKey
var setKey = h.setKey
var makeUId = h.makeUId
var _toTmpl = h._toTmpl
var checkVal = h.checkVal

var majordome = require('./majordome')
var red = majordome.red

var assert = require('assert')


function translateSingleValue (val, tArr, kind) {
  kind = typeof kind !== 'undefined' ? kind : 'Answer'
  //  console.log('kind',kind)
  if (val) {
    var cId = makeUId(kind)
    tArr.push({[cId]: val})
    val = _toTmpl(cId)
  }
  //  console.log(tArr)
  return val
}

/**
 * get the value of a key replace it with a uid
 * push the uid and value as an object
 * in the translation array
 * @param  {object}  obj        object to search
 * @param  {string}  key        to seah, can be nested
 * @param  {Array}   trArr      array of translated fields
 * @return {object}             modified obj
 */

function transKey (obj, key, trArr) {
  // console.log('la source', key,Array.isArray(trArr) )
  assert.ok(Array.isArray(trArr),
    red('you shoud provide an array to push trad to', trArr, obj))
  var trstring = getKey(obj, key)
  if (trstring && checkVal(trstring) === false) {
    var cId = makeUId(key)
    trArr.push({[cId]: trstring})
    setKey(obj, key, '<%= ' + cId + ' %>')
  }
  return obj
}

/**
 * apply transkey function to all items of the col
 * for each item of the fields array passed
 * @param  {obj/arr}  col  items to search for ke
 * @param  {arr}      fArr array of fields
 * @param  {arr}      tArr translation array
 */
function transFields (col, fArr, tArr) {
  _.forEach(fArr, function (field, index) {
    transKey(col, field, tArr)
  })
}

function transArray (mainArray, tArr) {
  _.forEach(mainArray, function (item, k) {
    if (typeof (item) !== 'string') {
      if (Array.isArray(item) === true) {
        return transArray(item, tArr)
      }
    } else {
      item = translateSingleValue(item, tArr)
      mainArray[k] = item
      return mainArray
    }
  })
}

/**
 * apply transfield to a collections
 */
function eachCol (col, fArr, tArr) {
  _.forEach(col, function (obj, k) {
    transFields(obj, fArr, tArr)
  })
  return tArr
}

module.exports = {
  tVal: translateSingleValue,
  tKey: transKey,
  tfArray: transArray,
  transFields: transFields,
  eachCol: eachCol
}
