
var _ = require('lodash')
/**
 * iterates the properties key of the schema and look for a translatable key,
 * if there is one, push it to the fields to translate array
 */

function getTrFields (obj, fArr, cb) {
  _.each(obj.properties, function (property, propertyName) {
    if (_.has(property, 'translatable') && property.translatable === true) {
      fArr.push(propertyName)
    }
  })
  if (fArr.length > 0) {
    return cb(fArr)
  } else return false
}

module.exports = getTrFields
