/**
 * textInput component js reader
 */
var _ = require('lodash')
var tObj = require('./../transformers')

var transArray = tObj.tfArray
var transFields = tObj.transFields
var eachCol = tObj.eachCol

var baseFields = [
  'title',
  'displayTitle',
  'body',
  'instruction',
  '_feedback.correct',
  '_feedback._incorrect.notFinal',
  '_feedback._incorrect.final',
  '_feedback._partlyCorrect.notFinal',
  '_feedback._partlyCorrect.final',
  'placeholder'
]

// textInput component can override buttons
var coreButtonsFields = [
  '_buttons._submit.buttonText',
  '_buttons._submit.ariaLabel',
  '_buttons._reset.buttonText',
  '_buttons._reset.ariaLabel',
  '_buttons._showCorrectAnswer.buttonText',
  '_buttons._showCorrectAnswer.ariaLabel',
  '_buttons._hideCorrectAnswer.buttonText',
  '_buttons._hideCorrectAnswer.ariaLabel',
  '_buttons._showFeedback.buttonText',
  '_buttons._showFeedback.ariaLabel',
  '_buttons.remainingAttemptsText',
  '_buttons.remainingAttemptText',
  '_buttons.disabledAriaLabel'
]

baseFields = baseFields.concat(coreButtonsFields)
module.exports = function (conf) {
  // console.log('textInput Module')
  transFields(conf.o, baseFields, conf.t)
  _.forEach(conf.o._items, function (item) {
    // TODO check if it is only a sting array or not ? (doc say yes ...)
    transArray(item._answers, conf.t)
  })
  eachCol(conf.o._items, ['prefix', 'placeholder', 'suffix'], conf.t)
  return conf
}
