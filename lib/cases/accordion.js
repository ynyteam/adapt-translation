var debug = require('debug')('trad:accordion')
var tObj = require('./../transformers')
var eachCol = tObj.eachCol
var transFields = tObj.transFields

var baseFields = [
  'title',
  'displayTitle',
  'instruction',
  'body'
]
var itemFields = [
  'title',
  'body',
  '_graphic.alt'
]

module.exports = function (conf) {
  debug('begin translation')
  transFields(conf.o, baseFields, conf.t)
  eachCol(conf.o._items, itemFields, conf.t)
  return conf
}
