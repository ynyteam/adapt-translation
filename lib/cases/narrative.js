/**
 * narrative component js reader
 */
var _ = require('lodash')
var tObj = require('./../transformers')

var transFields = tObj.transFields
var eachCol = tObj.eachCol

var assert = require('assert')
var i = require('../majordome')
var red = i.red

var baseFields = [
  'title',
  'displayTitle',
  'body',
  'instruction',
  'mobileInstruction'
]

var itemArr = [
  'title',
  'body',
  '_graphic.alt',
  'strapline']

module.exports = function (conf) {
  assert.ok(typeof conf.o === 'object',
    red('non standard conf fill, missing data narrative'))
  transFields(conf.o, baseFields, conf.t)
  _.forEach(conf.o._items, function (item, iter) {
    transFields(item, itemArr, conf.t)

    eachCol(item._options, ['text'], conf.t)
  })

  return conf
}
