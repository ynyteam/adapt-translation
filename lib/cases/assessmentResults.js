var tObj = require('./../transformers')
var transFields = tObj.transFields
var eachCol = tObj.eachCol
// var fs = require('fs-extra')
// var path = require('path')
// var testData = fs.readJsonSync(path.join(__dirname, '../../test/testFiles/samples/assessmentResults.json'))

// var daConf = {
//   o: testData,
//   t: []
// }

var baseFields = [
  'title',
  'displayTitle',
  'body',
  'instruction',
  '_assessmentId',
  '_retry.button',
  '_retry._comment',
  '_retry.feedback',
  '_comment',
  '_completionBody'
]

var bandsFields = [
  'feedback'
]

function transform (conf) {
  transFields(conf.o, baseFields, conf.t)

  if (conf.o._bands) eachCol(conf.o._bands, bandsFields, conf.t)

  return conf
}

// var daTest = transform(daConf)
// console.log(daTest.o)

module.exports = transform

module.exports.fields = baseFields
