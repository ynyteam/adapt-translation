/**
 * mcq component js reader
 */
var tObj = require('./../transformers')
var transFields = tObj.transFields

var assert = require('assert')
var i = require('../majordome')
var red = i.red

var baseFields = [
  'title',
  'displayTitle',
  'body',
  'instruction',
  '_feedback.correct',
  '_feedback._incorrect.notFinal',
  '_feedback._incorrect.final',
  '_feedback._partlyCorrect.final',
  '_feedback._partlyCorrect.notFinal',
  '_transcript.inlineTranscriptButton',
  '_transcript.inlineTranscriptCloseButton',
  '_transcript.inlineTranscriptBody',
  '_transcript.transcriptLinkButton'
]

module.exports = function (conf) {
  assert.ok(typeof conf.o === 'object',
    red('non standard conf fill, missing data'))
  transFields(conf.o, baseFields, conf.t)
  return conf
}
