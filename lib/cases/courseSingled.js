var tObj = require('./../transformers')
var transFields = tObj.transFields
var eachCol = tObj.eachCol
var chalk = require('chalk')
var red = chalk.red

const lRed = (text) => (console.log(red(text)))

var baseFields = [
  'title',
  'displayTitle',
  'description',
  'body',
  '_buttons._submit.buttonText',
  '_buttons._submit.ariaLabel',
  '_buttons._reset.buttonText',
  '_buttons._reset.ariaLabel',
  '_buttons._showCorrectAnswer.buttonText',
  '_buttons._showCorrectAnswer.ariaLabel',
  '_buttons._hideCorrectAnswer.buttonText',
  '_buttons._hideCorrectAnswer.ariaLabel',
  '_buttons._showFeedback.buttonText',
  '_buttons._showFeedback.ariaLabel',
  '_buttons.remainingAttemptsText',
  '_buttons.remainingAttemptText',
  '_buttons.disabledAriaLabel',
  '_globals._components._accordion.ariaRegion',
  '_globals._components._gmcq.ariaRegion',
  '_globals._components._hotGraphic.ariaRegion',
  '_globals._components._matching.ariaRegion',
  '_globals._components._mcq.ariaRegion',
  '_globals._components._media.ariaRegion',
  '_globals._components._narrative.ariaRegion',
  '_globals._components._slider.ariaRegion',
  '_globals._components._text.ariaRegion',
  '_globals._components._textInput.ariaRegion',
  '_globals._extensions._pageLevelProgress.pageLevelProgress',
  '_globals._extensions._pageLevelProgress.pageLevelProgressEnd',
  '_globals._extensions._pageLevelProgress.pageLevelProgressIndicatorBar',
  '_globals._extensions._pageLevelProgress.pageLevelProgressMenuBar',
  '_globals._extensions._pageLevelProgress.optionalContent',
  '_globals._menu._boxmenu.ariaRegion',
  '_globals._menu._boxmenu.menuItem',
  '_globals._menu._boxmenu.menuEnd',
  '_globals._accessibility._accessibilityToggleTextOn',
  '_globals._accessibility._accessibilityToggleTextOff',
  '_globals._accessibility._accessibilityInstructions.touch',
  '_globals._accessibility._accessibilityInstructions.notouch',
  '_globals._accessibility._accessibilityInstructions.ipad',
  '_globals._accessibility._ariaLabels.navigation',
  '_globals._accessibility._ariaLabels.menuLoaded',
  '_globals._accessibility._ariaLabels.menu',
  '_globals._accessibility._ariaLabels.page',
  '_globals._accessibility._ariaLabels.pageLoaded',
  '_globals._accessibility._ariaLabels.pageEnd',
  '_globals._accessibility._ariaLabels.previous',
  '_globals._accessibility._ariaLabels.navigationBack',
  '_globals._accessibility._ariaLabels.navigationDrawer',
  '_globals._accessibility._ariaLabels.close',
  '_globals._accessibility._ariaLabels.closeDrawer',
  '_globals._accessibility._ariaLabels.closeResources',
  '_globals._accessibility._ariaLabels.resourcesEnd',
  '_globals._accessibility._ariaLabels.drawerBack',
  '_globals._accessibility._ariaLabels.drawer',
  '_globals._accessibility._ariaLabels.closePopup',
  '_globals._accessibility._ariaLabels.next',
  '_globals._accessibility._ariaLabels.done',
  '_globals._accessibility._ariaLabels.complete',
  '_globals._accessibility._ariaLabels.incomplete',
  '_globals._accessibility._ariaLabels.incorrect',
  '_globals._accessibility._ariaLabels.correct',
  '_globals._accessibility._ariaLabels.correct',
  '_globals._accessibility._ariaLabels.locked',
  '_globals._accessibility._ariaLabels.accessibilityToggleButton',
  '_globals._accessibility._ariaLabels.feedbackPopUp',
  '_globals._accessibility._ariaLabels.menuBack',
  '_globals._accessibility._ariaLabels.visited',
  '_pageLevelProgress._altText',
  '_bookmarking.title',
  '_bookmarking.body',
  '_bookmarking._buttons.yes',
  '_bookmarking._buttons.no',
  '_resources.title',
  '_resources.description',
  '_resources._filterButtons.all',
  '_resources._filterButtons.document',
  '_resources._filterButtons.media',
  '_resources._filterButtons.link',
  '_resources._filterAria.allAria',
  '_resources._filterAria.documentAria',
  '_resources._filterAria.mediaAria',
  '_resources._filterAria.linkAria',
  '_resources.itemAriaExternal'
]

var resourcesFields = [
  'title',
  'description'
]

var extendId = [
  'textDest',  // holder for the translated text
  'wcOrig',   // word count in the orig lang
  'wcDest',   // word count in the destLang
  'charOrig', // character count in the orig lang
  'charDest' // character count in the destLang
]

module.exports = function (conf) {
  var trad = []
  transFields(conf.o, baseFields, trad)

  if (conf.o._resources) {
    lRed('RESOURCES')
    eachCol(conf.o._resources._resourcesItems, resourcesFields, trad)
    // conf.t.push(ressourcesArr)
  }

  var newT = trad.map(function (entry) {
    var newEntry = {}
    newEntry.compId = conf.id
    newEntry.id = Object.keys(entry)[0]
    var key = newEntry.id.replace(/\d/g, '')
    // console.log('key', key)
    newEntry.key = key
    newEntry.textOrig = entry[newEntry.id]

    // console.log('new key', newEntry.key)
    extendId.forEach(function (prop) {
      newEntry[prop] = ''
    })
    return newEntry
    // console.log('hahaha', newEntry)
    // extendId.forEach()
  })
  conf.trads = newT
  // console.log(conf.trads)
  // console.log('init O', initObj)
  return conf
}

module.exports.fields = baseFields
