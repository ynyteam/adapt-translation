var debug = require('debug')('treatFile')
var _ = require('lodash')

// create an object with the item serialised data, and some extracted info (id, parent and type)
function treatObj (item, index) {
  var treated = {}
  if (item._type) {
    treated.type = item._type
    treated.id = item._id
  } else if (item._defaultLanguage) {
    treated.type = 'config'
    treated.id = 'configId'
  }
  treated.serialised = JSON.stringify(item)

  if (item._type === 'component') {
    treated.cType = item._component
  }
  if (index !== undefined) { treated.index = index }
  if (item._parentId) {
    treated.parentId = item._parentId
  }
  return treated
}

// iterate in all object of the json to prepare them for translation extraction

function treatFile (data) {
  var fileItems = []
  // check if the data is an array if it isn't treat the data as object (courses are only object)
  if (Array.isArray(data)) {
    debug('data is an Array of item')
    _.each(data, function (item, index) {
      fileItems.push(treatObj(item, index))
    })
  } else {
    debug('data is a single obj')
    fileItems.push(treatObj(data))
  }
  debug('file treated')
  return fileItems
}

module.exports = treatFile
